package net.merryservices.mycardsapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.google.android.flexbox.FlexboxLayout;

import net.merryservices.mycardsapp.models.Card;
import net.merryservices.mycardsapp.models.Deck;
import net.merryservices.mycardsapp.view.CardView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Deck deck= new Deck();
        deck.shuffle();
        FlexboxLayout flexbox = (FlexboxLayout) findViewById(R.id.flexboxCards);

        while (deck.count()>0) {
            Card c = deck.draw();
            CardView cardView= new CardView(this, c);
            cardView.setOnClickListener(this);
            flexbox.addView(cardView);
        }
    }

    @Override
    public void onClick(View v) {
        CardView cardView = (CardView) v;
        cardView.getCard().setTurn(!cardView.getCard().isTurn());
        cardView.invalidate();
    }
}
