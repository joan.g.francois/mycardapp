package net.merryservices.mycardsapp.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.View;

import androidx.annotation.Nullable;

import net.merryservices.mycardsapp.models.Card;

public class CardView extends View {

    private Card card;

    public CardView(Context context, Card card) {
        super(context);
        this.card= card;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint paint = new Paint();
        paint.setColor(Color.BLUE);
        canvas.drawRect(0, 0, 200, 300, paint);
        paint.setColor(Color.WHITE);
        canvas.drawRect(10, 10, 190, 290, paint);
        paint.setTextSize(34);
        paint.setAntiAlias(true);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTypeface(Typeface.create("Arial", Typeface.BOLD));
        if(card.isTurn()) {
            paint.setColor(Color.BLUE);
            canvas.drawText("Card", 100, 150, paint);
        }else{
            paint.setColor(getColorPaint());
            canvas.drawText(getColorSymbol(card.getColor()), 100, 150, paint);
            paint.setTextAlign(Paint.Align.RIGHT);
            canvas.drawText(card.getValue(), 40, 50, paint);
            canvas.scale(-1f, -1f, 40, 50);
            canvas.drawText(card.getValue(), -80, -150, paint);
        }
    }

    private int getColorPaint(){
        if(card.getColor()=="COEUR" || card.getColor()=="CARREAU"){
            return Color.RED;
        }else{
            return Color.BLACK;
        }
    }

    private String getColorSymbol(String value){
        switch (value){
            case "COEUR":
                return new String(Character.toChars(0x2665));
            case "CARREAU":
                return new String(Character.toChars(0x2666));
            case "PIQUE":
                return new String(Character.toChars(0x2660));
            case "TREFLE":
                return new String(Character.toChars(0x2663));
        }
        return "";
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(200,300);
    }
}

